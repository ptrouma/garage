
# variables
firstName = "Peter"
lastName = "Ouma"
favColor = "Blue"

# puts "Hi, my name is " + $firstName + " " + $lastName + ", favourite color is " + $favColor

# print "Hi, my name is " + $firstName + " " + $lastName + ", favourite color is " + $favColor

# numbers
num1 = 510
num2 = 50.0

sub = num1 - num2
# puts "Subtraction " + $sub.to_s

div = num1/num2
# puts "Division " + $div.to_s

mod = num1 % num2
# puts "Modulus " + $mod.to_s

#collections - Arrays
groceryList = ["Mango", "Orange", "Banana", "Pineapple"]
# puts $groceryList

arraySize = groceryList.size
# puts $arraySize

groceryList << "Tropical fruit"
# puts groceryList

favMovies = ["pelham", "salt"]

#collections - Hashes
info = {}
info["firstName"] = "Peter"
info["secondName"] = "Ouma"
info["Age"] = 27
info["city"] = "Nairobi"
info["grocery"] = groceryList
info["movies"] = favMovies

# puts info.methods

## - methods
def pretty_print(hash_name)
	puts " "
	hash_name.each do |k, v|
		puts v
	end
	puts " "
end

pretty_print(info)

## - booleans
favourite_color = "Blue"
not_fav_color = "Red"

## - loops
t = 0
while t < 10
	puts "("+t.to_s+")" + "Ruby rocks!"
	t = t+1
end

## -- object orientation
class Person

	name = "Peter"

	def print_name(name)
		puts name
	end

end
