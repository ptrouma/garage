## --my info-- ##
info = {}

info["firstName"] = "Peter"
info["middleName"] = "Ouma"
info["lastName"] = "Otieno"
info["year_of_birth"] = 1986
info["city"] = "Nairobi"
info["homeTown"] = "Mombasa"


def print_info(my_info)
	my_info.each do |k, i|
		puts i
	end
end

def calculate_age(year_of_birth)
	
	return 2015 - year_of_birth
end

def concat_names(first, middle)
	return first+ " " +middle
end

# -- calculate age
info["Age"] = calculate_age(info["year_of_birth"])
# -- get full names
info["fullNames"] = concat_names(info["firstName"], info["middleName"])

# -- print bio
#print_info(info)


class Person 

	def initialize(name, yob)
		@person_name = name
		@year_of_birth = yob
	end

	def calculate_age
		return 2015 - @year_of_birth
	end

	def get_name
		puts @person_name
	end

	def get_age
		puts calculate_age
	end

end

person = Person.new("Peter", 1986)

person.get_name
person.get_age

